const express = require('express')
var path = require('path');
const app = express()

app.use(express.static(path.join(__dirname, 'public')));

app.get('/', (req, res) => res.sendFile(path.join(__dirname + '/public/index.html')))

var port = process.env.PORT || 8080;
console.log('Websocket server running on port ' + port)

app.listen(port, () => console.log('Server running!'))